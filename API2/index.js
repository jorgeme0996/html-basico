window.onload = function() {
  console.log("Ya termine de cargar y voy a hacer algo");

  fetch('http://localhost:3000/users')
    .then(response => response.json())
    .then((data) => {
      console.log(data)
      hacerListaDePersonajes(data)
    });
};

function hacerListaDePersonajes(usuarios) {
  console.log('Estoy en la funcion hacerListaDePersonajes', usuarios);

  var listaDePersonajes = document.getElementById('lista-de-usuarios')

  for(usuario of usuarios) {
    console.log(usuario);
    listaDePersonajes.innerHTML += `
      <li>
        Persona: ${usuario.name}
        <ul>
          <li>
            <img src="${usuario.photo}" height="200" width="200">
          </li>
        </ul>
      </li>
      <br>
    `
  }
}