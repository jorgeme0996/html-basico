alert("Entraste a mi pagina, bienvenido");

var elParrafoEstaEscondido = false;

var elAnuncioEstaEscondido = true;

function handleClick() {
  var ElParrafo = document.getElementById("soy-el-parrafo");

  console.log(ElParrafo);
  if(elParrafoEstaEscondido) {
    console.log("Estoy escondido");
    elParrafoEstaEscondido = false;
    //ElParrafo.style.visibility = "visible";
    ElParrafo.style.display = "block";
  } else {
    console.log("Estoy mostrado");
    elParrafoEstaEscondido = true;
    //ElParrafo.style.visibility = "hidden"; // visibility: "hidden" esconde el elemento y no le quita el espacio de la pagina
    ElParrafo.style.display = "none"; // display: "none" esconde el elemento y le quita el espacio de la pagina
  }

}

function handleClick2() {
  var ElParrafo = document.getElementById("soy-el-parrafo");

  console.log(ElParrafo);

  ElParrafo.style.fontSize = "35px";
}

function cerrarOMostrarAnuncio() {
  var ElAnuncio = document.getElementById("anuncio");

  if(elAnuncioEstaEscondido) {
    elAnuncioEstaEscondido = false;
    ElAnuncio.style.display = "block";
  } else {
    elAnuncioEstaEscondido = true;
    ElAnuncio.style.display = "none";
  }
}