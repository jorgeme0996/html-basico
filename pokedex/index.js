window.onload = function() {
  console.log('Ya termine de cargar');

  fetch('https://pokeapi.co/api/v2/pokemon')
    .then(response => response.json())
    .then((data) => {
      console.log(data)
      insertarPokemonesASuCaja(data.results)
    });
}

function insertarPokemonesASuCaja(pokemones) {
  var cajaDePokemones = document.getElementById('cont-pokemones');

  for(pokemon of pokemones) {
    console.log(pokemon);

    // cajaDePokemones.innerHTML = cajaDePokemones.innerHTML + `
    //     <div class="card mt-5" style="width: 18rem;">
    //     <div class="card-body">
    //       <h5 class="card-title">${pokemon.name.toUpperCase()}</h5>
    //       <a href="#" class="btn btn-primary">Saber más</a>
    //     </div>
    //   </div>
    // `
    cajaDePokemones.innerHTML +=  `
      <div class="card mt-5" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">${pokemon.name.toUpperCase()}</h5>
          <a href="/pokedex/pokemon/${pokemon.name}" class="btn btn-primary">Saber más</a>
        </div>
      </div>
    `
  }
}

function resolverBusqueda(event) {
  event.preventDefault()

  var inputBusqueda = document.getElementById('buqueda');

  console.log(inputBusqueda.value);
}