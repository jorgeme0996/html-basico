window.onload = function() {
  fetch('https://pokeapi.co/api/v2/pokemon/ivysaur')
  .then(response => response.json())
  .then((data) => {
    console.log(data)
    insertarHabilidadesALista(data.abilities)
  });
}

function insertarHabilidadesALista(habilidades) {
  var listaHabilidades = document.getElementById('lista-habilidades')

  for(habilidad of habilidades) {
    console.log(habilidad);

    listaHabilidades.innerHTML += `
      <li>${habilidad.ability.name}</li>
    `
  }
}