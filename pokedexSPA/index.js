window.onload = function() {
    console.log('Ya termine de cargar');

    fetch('https://pokeapi.co/api/v2/pokemon')
        .then(response => response.json())
        .then((data) => {
        console.log(data)
            insertarPokemonesASuCaja(data.results)
        });
}

function insertarPokemonesASuCaja(pokemones) {
    console.log(pokemones);

    var cajaDePokemones = document.getElementById('cont-pokemones');

    for(pokemon of pokemones) {
        cajaDePokemones.innerHTML += `
            <div class="pokemon-card" id="${pokemon.name}">
                <p id="${pokemon.name}">${pokemon.name.toUpperCase()}</p>
            </div>
        `
    }
}

function handleClick(event) {
    var pokemon = event.target.id;
    fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
    .then(response => response.json())
    .then((data) => {
        console.log(data)
        mostrarInfoDelPoke(data)
    });
}

function mostrarInfoDelPoke(pokemonInfo) {
    var body = document.querySelector('body')

    var listaDeMovimientos = ""

    pokemonInfo.moves.forEach((move, i) => {
        listaDeMovimientos += `<div class="move">${i + 1}. ${move.move.name}</div>`
    })          

    body.innerHTML += `
        <div id="modal-pokemon">
            <div class="cont-title-close">
                <h1>${pokemonInfo.name}</h1>
                <p onclick="closeModal(event)">close X</p>
            </div>
            
            <h3>Movimientos:</h3>
            <div class="cont-habilidades">
                ${listaDeMovimientos}
            </div>
        </div>
    `

}

function closeModal(event) {
    console.log(event);
    document.getElementById('modal-pokemon').remove()
}