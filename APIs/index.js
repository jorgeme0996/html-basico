window.onload = function() {
  console.log("Ya termine de cargar y voy a hacer algo");

  fetch('https://swapi.dev/api/people/')
    .then(response => response.json())
    .then((data) => {
      console.log(data)
      hacerListaDePersonajes(data.results)
    });
};

function hacerListaDePersonajes(personajes) {
  console.log('Estoy en la funcion hacerListaDePersonajes', personajes);

  var listaDePersonajes = document.getElementById('lista-de-personajes')

  for(persona of personajes) {
    console.log(persona);
    listaDePersonajes.innerHTML += `
      <li>
        Persona: ${persona.name}
        <ul>
          <li>Altura: ${persona.height} m</li>
          <li>Color de Piel: ${persona.skin_color}</li>
        </ul>
      </li>
      <br>
    `
  }
}